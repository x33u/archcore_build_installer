#!/bin/sh

HOSTNAME=archcore
ROOT_PASSWD=rootpasswd
USER_1=user
USER_1_PASSWD=userpasswd
SSH_PUBKEY=AAA
SSH_PORT=3303
LOCALE_CONF=de_DE.UTF-8
LOCALE_GEN=de_DE
LOCALTIME=Europe/Berlin
KEYMAP=de-latin1
FONT=lat9w-16
NFTABLE_CONF="$(cat <<EOF
flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0; policy drop;
                # accept any localhost traffic
                iif lo accept
                # accept traffic originated from us
                ct state established,related accept
                # drop invalid packets
                ct state invalid counter drop
                # accept ssh, http, and https
                # tcp dport { 80, 443, 3303 } accept
                tcp dport ${SSH_PORT} 
                # accept icmp
                ip protocol icmp drop
                # count and reject everything else
                counter reject with icmpx type admin-prohibited
        }

        chain forward {
                type filter hook forward priority 0; policy drop;
        }

        chain output {
                type filter hook output priority 0; policy accept;
        }

}
EOF
)"

#-------------------------------------------------------------------------------

# configure
echo "${HOSTNAME}" > /etc/hostname
echo "LANG=${LOCALE_CONF}" > /etc/locale.conf
sed -i "s/^#${LOCALE_GEN}/${LOCALE_GEN}/" /etc/locale.gen
locale-gen
echo "KEYMAP=${KEYMAP}" > /etc/vconsole.conf
echo "FONT=${FONT}" >> /etc/vconsole.conf
ln -sf /usr/share/zoneinfo/${LOCALTIME} /etc/localtime
mkinitcpio -p linux

# bootloader
pacman -S gptfdisk syslinux --noconfirm
sed -i "s/\s*sda3/sda1/" /boot/syslinux/syslinux.cfg
sed -i "s|\s*/dev/sda3|p_archcore|" /boot/syslinux/syslinux.cfg
syslinux-install_update -iam

# user
echo "root:${ROOT_PASSWD}" | chpasswd
useradd -m -g users -s /bin/bash ${USER_1}
echo "${USER_1}:${USER_1_PASSWD}"  | chpasswd
mkdir /home/${USER_1}/.ssh
echo "ssh-rsa ${SSH_PUBKEY}" > /home/${USER_1}/.ssh/authorized_keys
chmod 700 /home/${USER_1}/.ssh
chown -R ${USER_1}: /home/${USER_1}/.ssh

# packages
pacman -S git \
          haveged \
          htop \
          mc \
          nano \
          linux \
          nftables \
          openssh \
          rsync \
          screen \
          wget \
          zsh-syntax-highlighting \
          zsh \
          --noconfirm

# services
systemctl enable dhcpcd.service haveged.service nftables.service sshd.service

# ssh - echo "" >> /etc/ssh/sshd_config
echo "Protocol 2" > /etc/ssh/sshd_config
echo "Port ${SSH_PORT}" >> /etc/ssh/sshd_config
echo "LoginGraceTime 30" >> /etc/ssh/sshd_config
echo "PermitRootLogin no" >> /etc/ssh/sshd_config
echo "AuthorizedKeysFile      .ssh/authorized_keys" >> /etc/ssh/sshd_config
echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config
echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
echo "ServerKeyBits 4096" >> /etc/ssh/sshd_config
echo "KeyRegenerationInterval 3600" >> /etc/ssh/sshd_config
echo "SyslogFacility AUTH" >> /etc/ssh/sshd_config
echo "LogLevel INFO" >> /etc/ssh/sshd_config
echo "AllowUsers ${USER_1}" >> /etc/ssh/sshd_config
echo "AddressFamily inet" >> /etc/ssh/sshd_config
echo "MaxAuthTries 3" >> /etc/ssh/sshd_config
echo "StrictModes yes" >> /etc/ssh/sshd_config
echo "IgnoreRhosts yes" >> /etc/ssh/sshd_config
echo "UseDNS no" >> /etc/ssh/sshd_config
echo "HostbasedAuthentication no" >> /etc/ssh/sshd_config
echo "ChallengeResponseAuthentication yes" >> /etc/ssh/sshd_config
echo "AuthenticationMethods publickey,keyboard-interactive" >> /etc/ssh/sshd_config
echo "Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes128-ctr" >> /etc/ssh/sshd_config
echo "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com" >> /etc/ssh/sshd_config
echo "HostKeyAlgorithms ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa" >> /etc/ssh/sshd_config
echo "KexAlgorithms curve25519-sha256@libssh.org" >> /etc/ssh/sshd_config
echo "X11Forwarding no" >> /etc/ssh/sshd_config
echo "PrintMotd no" >> /etc/ssh/sshd_config
echo "PrintLastLog yes" >> /etc/ssh/sshd_config
echo "TCPKeepAlive no" >> /etc/ssh/sshd_config
echo "UsePrivilegeSeparation yes" >> /etc/ssh/sshd_config
echo "UsePAM yes" >> /etc/ssh/sshd_config

# nftables
echo "${NFTABLE_CONF}" > /etc/nftables.conf

# copy userconf
# mv userconf.sh /home/${USER_1}

# clean up
sed -i '/chmod +x stage2.sh/d' /root/.bashrc
sed -i '/stage2.sh/d' /root/.bashrc
rm stage2.sh

# exit
echo "--------------------------------------------------------------"
echo ">> PLEASE exit && umount /dev/sda1 && shutdown -h 0 SYSTEM! <<"
echo "--------------------------------------------------------------"
exit
